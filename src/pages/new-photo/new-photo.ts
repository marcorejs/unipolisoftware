import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';

import { HomePage } from '../home/home';
import { Foto } from '../../commons/Foto';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import firebase from 'firebase';



@Component({
  selector: 'page-new-photo',
  templateUrl: 'new-photo.html',
})
export class NewPhotoPage {

  dbFotos: AngularFirestoreCollection<Foto>;
  descripcion: String = "";
  foto_tomada: any;
  lugar: String = "cargando...";
  foto: any;
  imagen_subida: boolean;
  opciones_camara: CameraOptions;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sheetCtrl: ActionSheetController,
    private camara: Camera,
    private geocoder: NativeGeocoder,
    private geolocation: Geolocation,
    private database: AngularFirestore,
    private toastCtrl: ToastController
  ) {
    this.dbFotos = this.database.collection<Foto>('fotos');
    this.imagen_subida = false;
    this.opciones_camara = {
      quality: 95,
      destinationType: this.camara.DestinationType.DATA_URL,
      sourceType: this.camara.PictureSourceType.CAMERA,
      encodingType: this.camara.EncodingType.JPEG,
      correctOrientation: false,
      allowEdit: true
    }
    
    this.geolocation.getCurrentPosition().then(position => {
      this.geocoder.reverseGeocode(position.coords.latitude, position.coords.longitude)
        .then((result: NativeGeocoderReverseResult) => this.lugar = result.thoroughfare)
        .catch((error: any) => console.log(error));
    })
  }

  ionViewDidLoad() {
    console.log(this.imagen_subida);
  }

  uploadPhoto(options: CameraOptions) {
    this.camara.getPicture(options).then(foto => {
      this.foto_tomada = foto;
      let storageRef = firebase.storage().ref('fotos/' + new Date().getTime());
      storageRef.putString(this.foto_tomada, 'base64', { contentType: 'image/jpeg' })
        .then(savedPicture => {
          this.foto = savedPicture.downloadURL;
          this.imagen_subida = true;
        })
    })
  }

  showActionSheetCamera() {
    let actionSheet = this.sheetCtrl.create({
      title: 'Fuente de la foto',
      buttons: [
        {
          text: 'Camara',
          handler: () => {
            this.uploadPhoto(this.opciones_camara);
          }
        }, {
          text: 'Galeria',
          handler: () => {
            this.opciones_camara.sourceType = this.camara.PictureSourceType.PHOTOLIBRARY;
            this.uploadPhoto(this.opciones_camara);
          }
        },
      ]
    })
    actionSheet.present();
  }

  confirmarFoto() {
    let toast = this.toastCtrl.create({
      message: "Foto publicada con exito",
      duration: 2000
    })
    let foto: Foto = {
      foto: this.foto,
      descripcion: this.descripcion,
      lugar: this.lugar,
      amors: 0
    }
    this.dbFotos.add(foto).then(foto =>{
      toast.present();
      this.navCtrl.setRoot(HomePage);
    })

  }
}
