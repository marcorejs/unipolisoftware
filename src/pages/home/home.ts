import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Foto } from '../../commons/Foto';
import { ViewPhotoPage } from './verFoto';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  fotos: Observable<Foto[]>;

  photoDoc: AngularFirestoreDocument<Foto[]>;
  photoCollectionRef: AngularFirestoreCollection<Foto[]>;

  constructor(
    public navCtrl: NavController,
    private database: AngularFirestore
  ) {
    this.photoCollectionRef = this.database.collection<Foto[]>('fotos');
    
    this.fotos = this.photoCollectionRef.snapshotChanges().map(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data() as Foto;
        const id = action.payload.doc.id;
        return { id, ...data };
      });
    });


  }

  darAmors(foto: Foto) {
    let amors_agregado = foto.amors + 1;
    this.photoCollectionRef.doc(foto.id).update({ amors: amors_agregado })
  }

  verFoto(_foto: Foto) {
    this.navCtrl.push(ViewPhotoPage, {
      id: _foto
    })
  }

  

}
