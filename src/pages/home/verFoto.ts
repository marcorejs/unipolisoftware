import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'verFoto.html'
})
export class ViewPhotoPage {

  foto = {}
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
    ) {
      this.foto = this.navParams.get('id');
  }

}